<?php

namespace Database\Seeders;

use App\Models\VideoCategory;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Auto\'s and Vehicles',
            'Funny Video\'s & Comedy',
            'Education',
            'Entertainment',
            'Film & Animation',
            'Gaming',
            'Howto & Style',
            'Music',
            'News & Politics',
            'Nonprofits & Activism',
            'People & Blogs',
            'Pets & Animals',
            'Science & Technology',
            'Sports',
            'Travel & Events',
        ];

        $pornCategories = [
            'Porn | Straight',
            'Porn | Gay',
            'Porn | Solo (men)',
            'Porn | Solo (woman)',
            'Porn | Group',
            'Porn | WTF!',
        ];

        foreach ($categories as $category) {
            VideoCategory::create([
                'name' => $category
            ]);
        }

        $pornCatId = 51;
        foreach ($pornCategories as $pornCategory) {
            VideoCategory::create([
                'id' => $pornCatId,
                'name' => $pornCategory
            ]);

            $pornCatId++;
        }
    }
}
