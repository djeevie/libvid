<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->string('title', 60);
            $table->string('description', 1000)->nullable();
            $table->integer('video_length_seconds');
            $table->date('creation_date')->nullable();

            $table->string('original_filename', 50);
            $table->string('new_filename', 50);
            $table->string('slug');
            $table->uuid('uuid');

            $table->boolean('published')->default(true);
            $table->boolean('public')->default(true);

            $table->unsignedBigInteger('video_category_id');
            $table->foreign('video_category_id')->references('id')->on('video_categories');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
