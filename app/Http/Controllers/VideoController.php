<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function showWatchVideoPage(): View
    {
        return view('video.watch');
    }

    public function showUploadPage(): View
    {
        return view('video.upload');
    }
}
