<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showHomePage(): View
    {
        return view('home.index');
    }

    public function showWatchPage(): View
    {

    }
}
