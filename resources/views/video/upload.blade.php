@extends('layouts.base')

@section('title')
    Upload Video
@endsection

@section('head-css')
    <link rel="stylesheet" href="{{ asset('css/uppy.min.css') }}">
@endsection

@section('head-scripts')
    <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet"/>

    <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
    <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
@endsection

@section('app')
    @include('layouts.partials.navbar')

    <div id="main" class="container py-5">
        <div class="row">
            <div class="col-12 px-0">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="h5">Upload A New Video To LibVid</h1>
                                <p class="text-black-50 small">
                                    On this page you can upload your video and edit it's
                                    information.<br>
                                    Please <strong>select</strong> or <strong>drag & drop</strong> your video file, and
                                    then provide your video with some more information.
                                </p>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div id="uppyFileUploadDropzone"></div>
                            </div>
                            <div class="col-12 col-md-8 mt-sm-3 mt-md-0">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="videoTitleTextInput">Title:</label>
                                            <input type="text" class="form-control" name="videoTitleTextInput"
                                                   id="videoTitleTextInput"
                                                   placeholder="Enter a Title..." disabled>
                                        </div>

                                        <div class="form-group">
                                            <label for="videoDescriptionTextArea">Video Description:</label>
                                            <textarea name="videoDescriptionTextArea" id="videoDescriptionTextArea"
                                                      class="form-control" cols="30" rows="2"
                                                      placeholder="Enter a description for your video here..."
                                                      disabled></textarea>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        @php
                                            if (\Illuminate\Support\Facades\Cache::has('video_categories')) {
                                                $videoCategories = \Illuminate\Support\Facades\Cache::get('video_categories');
                                            } else {
                                                $videoCategories = \App\Models\VideoCategory::all();
                                                \Illuminate\Support\Facades\Cache::forever('video_categories', $videoCategories);
                                            }
                                        @endphp

                                        <div class="form-group">
                                            <label for="videoCategorySelectInput">Select a category:</label>
                                            <select name="videoCategorySelectInput" id="videoCategorySelectInput"
                                                    class="form-control" disabled>
                                                <option disabled selected hidden>Select...</option>

                                                @php
                                                    $nsfwSeparationLineDrawn = false;
                                                @endphp
                                                @foreach($videoCategories as $videoCategory)
                                                    @if($videoCategory->id > 50 && !$nsfwSeparationLineDrawn)
                                                        <hr class="py-0">
                                                        @php
                                                            $nsfwSeparationLineDrawn = true;
                                                        @endphp
                                                    @endif
                                                    <option
                                                        value="{{ $videoCategory->id }}">{{ $videoCategory->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="publicPrivateToggleCheckbox" checked>
                                                <label class="custom-control-label" for="publicPrivateToggleCheckbox">Public/Not
                                                    Public</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button id="finishVideoUploadButton" class="btn btn-success float-right" disabled>Select a video
                            first...
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('layouts.partials.footer')
@endsection

@section('body-scripts')
    <script src="https://vjs.zencdn.net/7.8.4/video.js"></script>
    <script src="{{ asset('js/uppy.min.js') }}"></script>

    <script>
        var player = videojs('videoPlayer');
        player.aspectRatio('16:9');
        player.fluid(true);
    </script>

    <script>
        var uppy = Uppy.Core({autoProceed: false})
        uppy.use(Uppy.Dashboard, {target: '#uppyFileUploadDropzone', inline: true})
        uppy.use(Uppy.XHRUpload, {endpoint: '{{ url('api/upload') }}'})
    </script>
@endsection
