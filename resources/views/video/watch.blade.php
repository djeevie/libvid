@extends('layouts.base')

@section('title')
    Sheep Discovers How To Use A Trampoline
@endsection

@section('head-scripts')
    <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet"/>

    <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
    <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
@endsection

@section('app')
    @include('layouts.partials.navbar')

    <div id="main" class="container py-5">
        <div class="row">
            <div class="col-12 text-white">
                <div class="row">
                    <div class="col-12 mb-3 d-inline-flex">
                        <video
                            id="videoPlayer"
                            class="video-js vjs-default-skin vjs-big-play-centered vjs-block-error"
                            controls
                            preload="auto">
                            <source src="{{ asset('video/pexel4.mp4') }}" type="video/mp4"/>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a
                                web browser that
                                <a href="https://www.getfirefox.com" target="_blank">
                                    supports HTML5 video</a>
                            </p>
                        </video>
                        <span class="clearfix">&nbsp;</span>
                    </div>

                    <div class="col-12">
                        <h1 class="h4 font-weight-bold">A Beautiful Video Showing A Beach</h1>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-8">
                                <span class="small">16,234 views &centerdot; Sept 16, 2020</span>
                            </div>
                            <div class="col-4">
                                <button id="collapseVideoInformationButton" data-toggle="collapse"
                                        data-target="#videoInformationCollapsed"
                                        class="btn btn-link btn-sm float-right">Show more information...&nbsp;&blacktriangledown;
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-8">
                                <div id="videoInformationCollapsed" class="collapse mt-3">
                                    <strong>Video Description:</strong>
                                    <p class="small">At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                        blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas
                                        molestias excepturi sint occaecati cupiditate non provident, similique sunt in
                                        culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>

                                    <strong>Uploader:</strong>
                                    <p>Davey Velthove</p>

                                    <strong>Category:</strong>
                                    <p>Porn</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr class="border-secondary">

        <div class="row">
            <div class="col-12 col-lg-8 text-white small">
                <!-- Comments -->
                <div class="row">
                    <div class="col-12">
                        <h5>Comments</h5>
                    </div>

                    <div class="col-12 mt-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea id="commentTextArea"
                                              name="commentTextArea"
                                              class="form-control form-control-sm bg-dark text-white"
                                              rows="1"
                                              placeholder="Write comment..."></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="dropdown dropright float-left">
                                    <button type="button" class="btn btn-outline-light dropdown-toggle btn-sm"
                                            data-toggle="dropdown">
                                        Sort By&nbsp;&nbsp;&nbsp;
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item small" href="#">Most Recent First</a>
                                        <a class="dropdown-item small" href="#">Oldest First</a>
                                    </div>
                                </div>

                                <button class="btn btn-outline-primary btn-sm float-right">Post Comment</button>
                            </div>
                        </div>

                        <div class="row">
                            <div id="commentSection" class="col-12">
                                @for($comment = 0; $comment < 5; $comment++)
                                    <div class="row my-4">
                                        <div class="col-12 col-md-10">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit, sed do
                                            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                            veniam,
                                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat.
                                        </div>
                                        <div class="col-12 text-white-50 py-2">
                                            <span class="float-left">Rainbow Dash | 12:55 | 01-03-2020</span>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-12">
                                <button class="btn btn-outline-info w-100">Retrieve More Comments...</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 text-white small mt-4 mt-md-0">
                <div class="row mb-4">
                    <div class="col-12 h5 font-weight-bold">
                        Recommended
                    </div>
                </div>

                <div class="row">
                    @for($video = 0; $video < 10; $video++)
                        <div class="col-12 mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <a href="#">
                                        <img src="{{ asset('img/test/videothumbnail.jpg') }}"
                                             alt="Description" width="100%" class="rounded-lg">
                                    </a>
                                </div>
                                <div class="col-8 pl-0">
                                    <a href="#" class="font-weight-bold text-white h6">Test Video
                                        Number {{ $video }}</a><br>
                                    <span class="text-white-50">ILoveUnicorns</span><br>
                                    <span class="text-white-50">39M views | 12 years ago</span>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>

    @include('layouts.partials.footer')
@endsection

@section('body-scripts')
    <script src="https://vjs.zencdn.net/7.8.4/video.js"></script>

    <script>
        var player = videojs('videoPlayer');
        player.aspectRatio('16:9');
        player.fluid(true);
    </script>
@endsection
