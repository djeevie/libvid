<div id="topNavigationBar" class="navbar-dark opacity-dark-75 border-bottom border-dark shadow-lg fixed-top">
    <div class="container px-0">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-md">
                    <a class="navbar-brand" href="{{ url('/') }}"><span class="fas fa-video mr-2"></span>LibVid</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <form class="form-inline mx-auto my-2 my-md-0 px-0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    @php
                                        if (\Illuminate\Support\Facades\Cache::has('video_categories')) {
                                            $videoCategories = \Illuminate\Support\Facades\Cache::get('video_categories');
                                        } else {
                                            $videoCategories = \App\Models\VideoCategory::all();
                                            \Illuminate\Support\Facades\Cache::forever('video_categories', $videoCategories);
                                        }
                                    @endphp
                                    <button type="button" class="btn btn-secondary dropdown-toggle btn-sm"
                                            data-toggle="dropdown">
                                        Category
                                    </button>
                                    <div class="dropdown-menu">
                                        @php
                                            $nsfwSeparationLineDrawn = false;
                                        @endphp
                                        @foreach($videoCategories as $videoCategory)
                                            @if($videoCategory->id > 50 && !$nsfwSeparationLineDrawn)
                                                <hr class="py-0">
                                                @php
                                                    $nsfwSeparationLineDrawn = true;
                                                @endphp
                                            @endif
                                            <a class="dropdown-item small" href="#">{{ $videoCategory->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <input type="text" class="form-control form-control-sm" placeholder="Search...">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-secondary btn-sm" data-toggle="dropdown">
                                        <span class="fas fa-search"></span>
                                    </button>
                                </div>
                            </div>
                        </form>

                        <ul class="navbar-nav mr-0">
                            @guest
                                <li class="nav-item px-md-3 py-2 py-md-0">
                                    <a href="#" class="btn btn-link btn-sm text-white"><span
                                            class="fas fa-key mr-2"></span>
                                        Register</a>
                                </li>

                                <li class="nav-item py-2 py-md-0">
                                    <a href="#" class="btn btn-primary btn-sm"><span class="fas fa-user mr-2"></span>
                                        Login</a>
                                </li>
                            @else
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false">Dropdown</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
