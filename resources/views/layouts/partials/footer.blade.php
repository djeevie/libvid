<footer class="bg-dark text-white">
    <div class="container py-5">
        <div class="row">
            <div class="col-12">
                <span class="font-weight-bold">&copy; {{ date('Y') }} LibVid.com</span>
            </div>
        </div>
    </div>
</footer>
