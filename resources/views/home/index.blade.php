@extends('layouts.base')

@section('title')
    Home
@endsection

@section('app')
    @include('layouts.partials.navbar')

    <video autoplay muted loop id="homeBgVideo">
        <source src="{{ asset('video/pexel1.mp4') }}" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>

    <div id="homeJumbotron" class="jumbotron opacity-dark-75 py-5 mb-0 rounded-0 d-none d-lg-block mt-5">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h1 class="h3 text-white">The new, free alternative to YouTube.</h1>
                    <h3 class="h6 text-white-50">Without the censorship.</h3>
                </div>
                <div class="col-4 align-content-center text-right">
                    <a href="{{ route('video.upload') }}" class="btn btn-success btn-lg mt-2"><span class="fas fa-upload mr-4"></span>Start Uploading!
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div id="homeContent" class="opacity-dark-50 py-5 mt-5 mt-md-0">
        <div class="container">
            <div class="row text-white">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <h4>Latest Uploads</h4>
                        </div>
                    </div>

                    <div class="row">
                        @for($col = 0; $col <= 11; $col++)
                            <div class="col-12 col-md-6 col-lg-4 mb-4">
                                <div class="row">
                                    <div class="col-12 mb-2">
                                        <div class="bg-dark text-center" style="height: 120pt">
                                            <span class="fas fa-play-circle h1" style="margin-top: 15%"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 mx-2">
                                        <span class="font-weight-bold">Title of the video</span>
                                    </div>
                                    <div class="col-12 mx-2 small">
                                        <span class="font-weight-normal">Rainbow Dash</span>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('layouts.partials.footer')
@endsection
